#include "stdafx.h"
#include "LLQueue.h"

#include <iostream>

//create new Queue
LLQueue::LLQueue() {
	LLQ = new LList();
}


LLQueue::~LLQueue() {
}

//adds Node ->next
//adds Node after the first node, then after second, and so on
void LLQueue::enqueue(int value) {
	if (value < 999999999){
		ListNode * tempNode = LLQ->getHeadNode();
		LLQ->addNodeToEnd(value);
		count++;
	}
	else {
		std::cout << "Too big of a number." << std::endl;
	}
}

//remove the first item in queue
void LLQueue::dequeue() {
	ListNode * tempNode = LLQ->getHeadNode();
	if (tempNode != NULL) {
		LLQ->removeFirst();
		count--;
	}
}

//look at the value of the first item of queue
int LLQueue::peek() {
	ListNode * tempNode = LLQ->getHeadNode();
	if (tempNode == NULL) {
		std::cout << "Error: ";
		return 404;
	}
	else if (tempNode != NULL) {
		//std::cout << "Next Node is: " << tempNode->next << std::endl;
		std::cout << "Value: ";
		return tempNode->value;
	}
}

//how many items in queue
int LLQueue::getQuantity() {
	return count;
}

//prints the values
void LLQueue::printAll() {
	LLQ->printNode();
}