#include "stdafx.h"
#include "BinaryTree.h"
#include <iostream>


BinaryTree::BinaryTree() {
	root = NULL;
	count = 0;
}


BinaryTree::~BinaryTree() {

}

BTNode * BinaryTree::getRoot() {
	return root;
}

void BinaryTree::addNode(int value) {
	BTNode * newNode = new BTNode(value);

	//check if tree is empty
	if (root == NULL) {
		root = newNode; //set root to new node
	}
	else {
		BTNode * tempNode = root; //find place of entrance

		while(tempNode != NULL) {
			if (value < tempNode->value) {
				if (tempNode->leftChild == NULL) {
					tempNode->leftChild = newNode;
					newNode->parent = tempNode;
					break;
				}
				else {
					tempNode = tempNode->leftChild;
				}
			}
			else if (value > tempNode->value) {
				if (tempNode->rightChild == NULL) {
					tempNode->rightChild = newNode;
					newNode->parent = tempNode;
					break;
				}
				else {
					tempNode = tempNode->rightChild;
				}	
			}
		}
	}

	count++;
}

void BinaryTree::RemoveNode(int value) {
	BTNode * currentNode = root;

	BTNode * tempNode = root;

	BTNode * temp2Node = root;
	if (tempNode == NULL) {
		root = currentNode; //set root to new node
	}
	//when the value is less than the root of tree
	if (value < tempNode->value) {
		//search through for the value node
		while (tempNode->value != value) {
			tempNode = currentNode->leftChild;
			if (value < currentNode->value) {
				tempNode = currentNode->leftChild;
			}
			else if (value > currentNode->value) {
				tempNode = currentNode->rightChild;
			}
			//std::cout << tempNode->value << std::endl;
		}
		while (currentNode != NULL) {
			//in case right child is not empty of value node that is about to be deleted
			if (tempNode->rightChild != NULL) {
				temp2Node = tempNode->rightChild;
				temp2Node->parent = tempNode->parent;
				currentNode->parent = temp2Node;
				if (temp2Node->leftChild != NULL && temp2Node->leftChild->value < currentNode->value) {
					currentNode->leftChild = temp2Node->leftChild;
					temp2Node->leftChild->parent = currentNode;
					break;
					
				}
				else if (temp2Node->leftChild != NULL && temp2Node->leftChild->value > currentNode->value){
					currentNode->rightChild = temp2Node->leftChild;
					temp2Node->leftChild->parent = currentNode;
					break;
				}
			}
			//in case left child is not emprty of value node that is about to be deleted
			else if (tempNode->leftChild != NULL) {
				temp2Node = tempNode->parent;
				currentNode->parent = tempNode->parent;
				if (tempNode->rightChild != NULL && currentNode->value < temp2Node->value) {
					currentNode->rightChild = tempNode->rightChild;
					tempNode->rightChild->parent = currentNode;
					break;
					
				}
				else if (tempNode->rightChild != NULL && currentNode->value > temp2Node->value){
					currentNode->leftChild = tempNode->rightChild;
					tempNode->rightChild->parent = currentNode;
					break;

				}

			}
			std::cout << "Temp2Node: ";
			std::cout << temp2Node->parent->value;
			std::cout << temp2Node->leftChild->value;
			//std::cout << temp2Node->rightChild->value;

			std::cout << "TempNode: ";
			std::cout << tempNode->parent->value;
			std::cout << tempNode->leftChild->value;
			//std::cout << tempNode->rightChild->value;

			std::cout << "CurrentNode: ";
			std::cout << currentNode->parent->value;
			std::cout << currentNode->leftChild->value;
			//std::cout << currentNode->rightChild->value;

		}
		//delete tempNode;

	}
	//when the value is greater than the root of tree
	

}

void BinaryTree::printAll() {//HW implementation
	//always left then right
	//binary trees traversal
	BTNode * currentNode = root;
	BTNode * tempNode = currentNode->leftChild;

	BTNode * valueNode = root;

	/*std::cout << "Root: ";
	std::cout << currentNode->value << std::endl;*/

	if (currentNode->leftChild != NULL) {
		if (tempNode->leftChild != NULL) {
			valueNode = tempNode->leftChild;
			if (valueNode->leftChild != NULL) {
				tempNode = valueNode->leftChild;
			}
			else {
				std::cout << valueNode->value << std::endl;
			}
		}
		else if (tempNode->rightChild != NULL) {
			valueNode = tempNode->rightChild;
			if (valueNode->leftChild != NULL) {
				tempNode = valueNode->leftChild;
			}
			else {
				std::cout << valueNode->value << std::endl;
			}
		}
	}

	
	
	/*while (tempNode != NULL) {
		if (tempNode->leftChild->value < tempNode->value) {
			std::cout << "Left Node: " << std::endl;
			std::cout << tempNode->value << std::endl;
			//valueNode = tempNode->leftChild;
			std::cout << tempNode->leftChild->value << std::endl;
			break;
		}
	}
	while (tempNode != NULL){

		if (tempNode->rightChild->value > tempNode->value) {
			std::cout << "Right Left: " << std::endl;
				tempNode = tempNode->rightChild;
				std::cout << "Right Node: ";
				std::cout << tempNode->value << std::endl;
		}

	}*/
	
}

void BinaryTree::printPostOrder(BTNode * root) {
	if (root == NULL) return;
		printPostOrder(root->leftChild);
		printPostOrder(root->rightChild);

		std::cout << "Val: " << root->value << std::endl;
}

void BinaryTree::printInOrder(BTNode * root) {
	if (root == NULL) return;
		printInOrder(root->leftChild);

		std::cout << "Val: " << root->value << std::endl;

		printInOrder(root->rightChild);
}

void BinaryTree::printPreOrder(BTNode * root) {
	if (root == NULL) return;
	std::cout << "Val: " << root->value << std::endl;

	printInOrder(root->leftChild);
	printInOrder(root->rightChild);
}