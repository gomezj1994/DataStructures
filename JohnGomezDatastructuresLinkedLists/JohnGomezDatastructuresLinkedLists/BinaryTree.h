#pragma once
#include "BTNode.h"

class BinaryTree {
private:
	BTNode * root;
	int count;
public:
	BinaryTree();
	~BinaryTree();

	BTNode * getRoot();

	void addNode(int value);
	void RemoveNode(int value);
	void printAll();
	void printPostOrder(BTNode * root);
	void printInOrder(BTNode * root);
	void printPreOrder(BTNode * root);

};

