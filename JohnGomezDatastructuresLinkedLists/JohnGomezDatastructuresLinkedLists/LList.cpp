#include "stdafx.h"
#include "LList.h"
#include <iostream>


LList::LList() {
	//Point to the head of list
	//No head = NULL
	//Amount of Nodes in list
	head = NULL;
	count = 0;
}

LList::~LList() {
	ListNode * tempNode;
	while (head != NULL) {
			tempNode = head;
			head = head->next;
			delete tempNode;
		}
	
}

void LList::addNode(int value) {
	ListNode * newNode = new ListNode(value);
	newNode->next = head;
	head = newNode;
	count++;
}

void LList::addNodeToEnd(int value) {
	ListNode * newNode = new ListNode(value);

	ListNode * tempNode = head;

	if (isEmpty() == true) {//check for an empty list
		head = newNode;
	}
	else if (count == 1) {
		head->next = newNode;
	}
	else {
		while (tempNode->next != NULL) {//get last node of linked list
			tempNode = tempNode->next;
		}
		tempNode->next = newNode;
	}
	count++;
}

void LList::addToLocation(int value) {

}

void LList::addNodeAfterValue(int value, int locatedValue) {
	ListNode * newNode = new ListNode(value);

	ListNode * tempNode = head; 

	ListNode * aheadNode = tempNode->next;

	if (isEmpty() == true) {//check for an empty list
		head = newNode;
	}
	else if (count == 1) {
		head->next = newNode;
	}
	else {
		while ((tempNode->next != NULL) && (tempNode->value != locatedValue)) {//search for value in linked list

			tempNode = tempNode->next;
			aheadNode = tempNode->next;
		}
		tempNode->next = newNode;
		newNode->next = aheadNode;
	}
	count++;
}

ListNode * LList::getHeadNode() {
	ListNode * tempNode = head;
	return tempNode;
}

bool LList::removeFirst() {
	bool success = false;
	if (head != NULL) {
		ListNode * temp = head;
		head = head->next;
		delete temp;
		success = true;
	}
	return success;
}

void LList::removeNode(int value) {
	ListNode * tempNode = head;

	ListNode * previousNode = tempNode;

	ListNode * aheadNode = tempNode->next;

	if (isEmpty() == true)  {//check for an empty list

	}
	else if (count == 1) {
		head->next = tempNode;
		delete tempNode;
	}
	while ((tempNode->next != NULL) && (tempNode->value != value))  {//search for value in linked list
		previousNode = tempNode;
		tempNode = tempNode->next;
		aheadNode = tempNode->next;
	}
	previousNode->next = aheadNode;

	delete tempNode;

	count--;
}

void LList::printNode() {
	ListNode * currentNode = head;
	while (currentNode != NULL) {
		std::cout << currentNode->value << std::endl;
		currentNode = currentNode->next;
	}
}

bool LList::isEmpty() {
	bool success = true;
	if (count == 0) {
		return success;
	}
	else if (count >= 0) {
		success = false;
		return success;
	}
}

