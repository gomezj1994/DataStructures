#pragma once

#include "ListNode.h"

class LList {
private:
	ListNode * head;
	int count;
public:
	LList();
	~LList();

	void addNode(int value);
	void addNodeToEnd(int value);
	void addToLocation(int value);
	void addNodeAfterValue(int value, int locatedValue);
	ListNode * getHeadNode();
	bool removeFirst();
	void removeNode(int value);
	void printNode();
	bool isEmpty();
};


