// JohnGomezDatastructuresLinkedLists.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LList.h"
#include "LLStack.h"
#include "LLQueue.h"
#include "BinaryTree.h"

#include <iostream>

void LinkedListTest();
void StackListTest();
void QueueTest();
void BinaryTreeTest();

int main(){
	int input = 9;
	while (input != 0) {
		std::cout << "Input 1: LinkedList Test|Input 2: StackList Test|Input 3: QueueList" << std::endl;
		std::cout << "Input 4: BinaryTree Test|Input 0: EXIT" << std::endl;
		std::cin >> input;

		switch (input) {
		case 1:
			LinkedListTest();
			break;
		case 2:
			StackListTest();
			break;
		case 3:
			QueueTest();
			break;
		case 4:
			BinaryTreeTest();
			break;
		case 0:
			break;
		default:
			std::cout << "Incorrect Input" << std::endl;                                                                                                                                                                                                                                                                                                                         
			break;
		}
	}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      }

void LinkedListTest() {
	LList * LinkedList = new LList();
	LinkedList->addNode(5);
	LinkedList->addNode(4);
	LinkedList->addNode(3);
	LinkedList->addNode(2);
	LinkedList->addNode(1);

	LinkedList->addNodeToEnd(7);

	std::cout << "Printing list after adding value to end:" << std::endl;
	LinkedList->printNode();
	

	system("pause");

	LinkedList->addNodeAfterValue(6, 5);

	std::cout << "Printing list after adding a value after a existing value:" << std::endl;
	LinkedList->printNode();

	system("pause");

	LinkedList->removeNode(4);

	std::cout << "Printing list after removing:" << std::endl;
	LinkedList->printNode();

	system("pause");

}

void StackListTest() {
	LLStack * StackList = new LLStack();
	//adding a node 
	StackList->push(5);
	StackList->push(4);
	StackList->push(3);
	StackList->push(2);
	StackList->push(30000000000000);
	StackList->push(-588888888);

	for (int i = 0; i <= 25; i++) {
		StackList->push(i);
	}

	std::cout << "Items in your list: " << StackList->getLength() << std::endl; //checking the stack list
	std::cout << "Values in your list: " << std::endl; //printing the list
	StackList->printAll();

	system("pause");

	std::cout << "Top Item in your list: " << std::endl; 
	//peeking top item in list
	std::cout << StackList->peek() << std::endl;
	//StackList->peek(); No print in function

	system("pause");

	//removing a node
	std::cout << "Removing Items . . . " << std::endl;
	for (int i = 0; i <= 50; i++) {
		StackList->pop();
	}

	std::cout << "Items in your list: " << StackList->getLength() << std::endl; //checking the stack list after removing
	std::cout << "Values in your list: " << std::endl; //printing the list after removing
	StackList->printAll();

	system("pause");


}

void QueueTest() {
	LLQueue * QueueList = new LLQueue();
	//adding to the queue
	QueueList->enqueue(1);
	QueueList->enqueue(2);
	QueueList->enqueue(3);
	QueueList->enqueue(4);
	QueueList->enqueue(300000000);
	QueueList->enqueue(-588888888);
	
	for (int i = 0; i <= 125; i++) {
		QueueList->enqueue(i);
	}

	std::cout << "Items in your queue: " << QueueList->getQuantity() << std::endl; //checking the queue
	std::cout << "Values in your queue: " << std::endl; //printing the queue
	QueueList->printAll();

	system("pause");

	std::cout << "First Item in your queue: " << std::endl;
	//peeking top item in list
	std::cout << QueueList->peek() << std::endl;

	system("pause");

	//removing a node
	std::cout << "Removing Items . . . " << std::endl;
	
	for (int i = 0; i <= 150; i++) {
		QueueList->dequeue();
	}


	std::cout << "Items in your queue: " << QueueList->getQuantity() << std::endl; //checking the queue after removing
	std::cout << "Values in your queue: " << std::endl; //printing the queue after removing 
	QueueList->printAll();

	system("pause");

}

void BinaryTreeTest() {
	BinaryTree * BTree = new BinaryTree();
	BTree->addNode(2);
	BTree->addNode(1);
	BTree->addNode(0);

	//printing all node in tree
	/*BTree->printAll();

	system("pause");*/

	std::cout << "Post-Order: " << std::endl;
	BTree->printPostOrder(BTree->getRoot());

	system("pause");

	std::cout << "In-Order: " << std::endl;
	BTree->printInOrder(BTree->getRoot());

	system("pause");

	std::cout << "Pre-Order: " << std::endl;
	BTree->printPreOrder(BTree->getRoot());

	system("pause");

	std::cout << "Removing Node of Value: 1" << std::endl;
	BTree->RemoveNode(1);

	system("pause");

	std::cout << "Post-Order: " << std::endl;
	BTree->printPostOrder(BTree->getRoot());

	system("pause");

	std::cout << "In-Order: " << std::endl;
	BTree->printInOrder(BTree->getRoot());

	system("pause");

	std::cout << "Pre-Order: " << std::endl;
	BTree->printPreOrder(BTree->getRoot());

	system("pause");


}