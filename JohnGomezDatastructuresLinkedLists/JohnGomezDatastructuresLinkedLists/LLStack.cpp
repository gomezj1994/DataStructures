#include "stdafx.h"
#include "LLStack.h"

#include <iostream>

//create new stack
LLStack::LLStack() {
	LL = new LList();
}


LLStack::~LLStack() {
	delete LL;
}

//adds Node at head position 
//adds at headnode then next added node adds to the head and so on
void LLStack::push(int value) { 
	if (value < 999999999) {
		LL->addNode(value);
		count++;
	}
	
}

//removes the first item in Stack
void LLStack::pop() {
	ListNode * tempNode = LL->getHeadNode();
	if (tempNode != NULL) {
		LL->removeFirst();
		count--;
	}
}

//looks at the first item in stack
int LLStack::peek() {
	//bool success = NULL;

	ListNode * tempNode = LL->getHeadNode();
	if (tempNode == NULL) {
		std::cout << "Error: ";
		return 404;
	}
	else if (tempNode != NULL) {
		//std::cout << "Next Node is: " << tempNode->next << std::endl;
		std::cout << "Value: ";
		return tempNode->value;
	}
}

//how many items in stack
int LLStack::getLength() {
	return count;
}

//prints items in stack
void LLStack::printAll() {
	LL->printNode();
}