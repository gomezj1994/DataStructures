#pragma once
#include "LList.h"


class LLQueue {
private:
	int count;
	LList * LLQ;
public:
	LLQueue();
	~LLQueue();

	void enqueue(int value);
	void dequeue();
	int peek();
	int getQuantity();
	void printAll();

};

