#pragma once

class ListNode {//Pointer and value is what needed for a node in a linked list

public:
	ListNode * next; //pointer to next node
	int value; //value of current node

	ListNode(int val) { next = NULL; value = val; }
};
