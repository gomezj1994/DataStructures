#pragma once
class BTNode {
public:
	BTNode * parent;
	BTNode * leftChild;
	BTNode * rightChild;
	int value;

	BTNode(int value);
	~BTNode();
};

