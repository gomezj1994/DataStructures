#pragma once
#include "LList.h"

class LLStack {
private:
	int count;
	LList * LL;

public:
	LLStack();
	~LLStack();

	void push(int value);
	void pop();
	int peek();
	int getLength();
	void printAll();


};

